Additional info
https://kind.sigs.k8s.io/docs/user/quick-start/


Steps
1. install docker
2. docker run --name k8s-server --privileged -t -p 30000:30100 -p 30001:8000 -i jpetazzo/dind
3. mkdir /usr/local/kind
4. curl -Lo /usr/local/kind/kind https://kind.sigs.k8s.io/dl/v0.8.1/kind-linux-amd64
5. chmod +x /usr/local/kind/kind
6. export PATH=$PATH:/usr/local/kind
7. kind create cluster --name k8s-poc-cluster --config poc-cluster-config.yaml(wait for creating the cluster...)
8. curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
9. chmod +x ./kubectl
10. mv ./kubectl /usr/local/bin/kubectl
11. kubectl cluster-info k8s-poc-cluster (to get status of the cluster)
12. wget https://dl.google.com/go/go1.10.3.linux-amd64.tar.gz
13. tar -C /usr/local -xzf go1.10.3.linux-amd64.tar.gz
14. export PATH=$PATH:/usr/local/go/bin
15. sudo apt-get update
16. sudo apt-get install git
17. wget https://github.com/derailed/k9s/releases/download/v0.21.2/k9s_Linux_x86_64.tar.gz
18. tar -C /usr/local/k9s -xzf k9s_Linux_x86_64.tar.gz  
19. export PATH=$PATH:/usr/local/k9s
20. alias k=kubectl
21. source <(kubectl completion bash)
22. echo "source <(kubectl completion bash)" >> ~/.bashrc
23. k create -f ./poc-namespace.yaml
24. k apply -f . --namespace=poc-k8s


FAQ
1. ports in service
Port exposes the Kubernetes service on the specified port within the cluster.
 Other pods within the cluster can communicate with this server on the specified port.
TargetPort is the port on which the service will send requests to, that your pod will be listening on. 
Your application in the container will need to be listening on this port also.
NodePort exposes a service externally to the cluster by means of the target nodes IP address and the NodePort. 
NodePort is the default setting if the port field is not specified.
2. oracle db
oracle defaults
hostname: localhost
port: 1521
sid: xe
service name: xe
username: system
password: oracle


mkdir /usr/local/kind
curl -Lo /usr/local/kind/kind https://kind.sigs.k8s.io/dl/v0.8.1/kind-linux-amd64
chmod +x /usr/local/kind/kind
export PATH=$PATH:/usr/local/kind
kind create cluster --name k8s-poc-cluster --config poc-cluster-config.yaml
curl -LO https://storage.googleapis.com/kubernetes-release/release/`curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt`/bin/linux/amd64/kubectl
chmod +x ./kubectl
mv ./kubectl /usr/local/bin/kubectl
kubectl cluster-info k8s-poc-cluster (to get status of the cluster)
docker pull quay.io/maksymbilenko/oracle-12c 
kind load docker-image quay.io/maksymbilenko/oracle-12c:latest --name k8s-poc-cluster
wget https://dl.google.com/go/go1.10.3.linux-amd64.tar.gz
tar -C /usr/local -xzf go1.10.3.linux-amd64.tar.gz
export PATH=$PATH:/usr/local/go/bin
sudo apt-get update
sudo apt-get install git
wget https://github.com/derailed/k9s/releases/download/v0.21.2/k9s_Linux_x86_64.tar.gz
tar -C /usr/local/k9s -xzf k9s_Linux_x86_64.tar.gz  
export PATH=$PATH:/usr/local/k9s
alias k=kubectl
source <(kubectl completion bash)
echo "source <(kubectl completion bash)" >> ~/.bashrc
